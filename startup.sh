#!/bin/bash

# if variable develop exists set config
[[ ! -z $develop ]] && echo "os.environ[\"develop\"] = '$develop'" >> /var/www/UserManagement/UserManagementApp/config.py

service apache2 start

# keeps container running while service is running
while [[ `service apache2 status` == *"is running"* ]]; do
	sleep 1m
done
