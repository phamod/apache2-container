# openldap-container

creates a basic ldap server for user management

## Quickstart
build the image
> docker build -t slapd .  

run the image and save configuration to /srv/slapd/etc and /srv/slapd/var
> docker run -p 389:389 -v /srv/slapd/etc:/etc/ldap -v /srv/slapd/var:/var/lib/ldap slapd

## Setup
On first run an ldap configuration will be made creating .slapdconf files in the following locations: /etc/ldap and /var/lib/ldap

## Container Environments varibles
This contains a list of environment variables and their uses
example use
> docker run -p 389:389 -e LDAP_PASSWORD='secret'

1. LDAP_PASSWORD password for the admin user default is password please change
2. LDAP_DOMAIN domain for the server by default it's example.com
3. LDAP_ORGANIZATION adds organization name to the database
4. LDAP_BACKEND choices of BDB, HDB and MDB the default and recommended is MDB 
5. LDAP_USER common name for the original admin user default hamod
6. LDAP_USER_PASSWORD password for the admin user not root default password

## Ldap configuration explanation
* Passwords can only be accessed by owner or admin or for authorization otherwise hide it 
* root user is hidden from all user searches
* users of the admin group can modify the user dirctory adding users, deleting users, lock/unlock accounts, change passwords
* everyone can search all directories not hidden by above rules   

## TODO
* add liscence to repo
* give admin user access to roles directory
* give admin access to policies directory
* upgrade to ldaps protocol /etc/ldap/ldap.conf will be important
* add variable and check to reconfigure from scratch