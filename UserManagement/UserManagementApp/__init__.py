from flask import Flask, Blueprint, request, jsonify, make_response
from usermanager import UserManager
from tokenmanager import TokenManager
import json
import os, logging
from  . import config

logging.error('starting up python server')
print('testing print')
print(os.environ)

app = Flask(__name__)

# if this container is being used develop allow cors
if 'develop' in os.environ:
    from flask_cors import CORS
    print('running in develop mode')
    CORS(app)


key = 'secret'
tokenManager = TokenManager(key)

@app.route("/")
def hello():
    response = {}
    id = UserManager.parse_id_from_dn(
        request.environ.get("SSL_CLIENT_S_DN"))
    userManager = UserManager(
        'ldap://172.17.0.1:389', 'cn=hamod,ou=users,dc=example,dc=com', 'password')
    dn = userManager.get_user_dn_from_id(id)
    print(dn)
    roles = []
    if dn != None:
        roles = userManager.get_roles(dn)
        user = userManager.get_role_from_dn(dn)
        token = tokenManager.generate_refresh_token(user, roles)
        response = app.response_class(response=json.dumps({'token': token.decode("utf-8")}),mimetype='application/json',status=200)
    else:
        response = app.response_class(response='unauthorized',status=401)
    print(response)

    return response


if __name__ == "__main__":
    app.run()

bp = Blueprint('burritos', __name__)


@bp.route("/")
def index_page():
    return "This is a website about burritos"


@bp.route("/about")
def about_page():
    return "This is a website about burritos"


app.register_blueprint(bp, url_prefix='/0.0.1')
