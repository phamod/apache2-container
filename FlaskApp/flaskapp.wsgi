#!/usr/bin/python
import sys
import logging

logging.basicConfig(stream=sys.stderr)


from user_routes import app as application
application.secret_key = 'Add your secret key'
