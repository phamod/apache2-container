# FROM localhost:8082/debian:local

# RUN curl -O 172.17.0.1:8081/repository/files/public.gpg.key
# RUN apt-key add public.gpg.key && apt-get update
# RUN rm public.gpg.key

FROM debian:latest

WORKDIR /usr/src/app
RUN apt-get update -y
RUN apt-get install -y vim python python3-pip apache2 libapache2-mod-wsgi-py3 curl libldap2-dev libsasl2-dev dos2unix
COPY . .
RUN a2dissite 000-default.conf
RUN mv FlaskApp /var/www/
RUN mv UserManagement /var/www

# enable pages on http
RUN cp ./http.conf /etc/apache2/sites-available
RUN a2ensite http

# enable pages on https
RUN cp ./smartcard.conf /etc/apache2/sites-available
RUN a2enmod ssl
RUN a2ensite smartcard
RUN mkdir -p /etc/apache2/ssl/certs
RUN openssl req -x509 -nodes -days 1095 -newkey rsa:2048 -out /etc/apache2/ssl/server.crt -keyout /etc/apache2/ssl/server.key \
-subj "/C=US/ST=SC/L=Greenville/O=Internet Widgits Pty Ltd/CN=example.com/emailAddress=pfhamod@gmail.com"
RUN openssl pkcs7 -in Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD.pem.p7b -print_certs -out alldodcerts.pem
RUN mv alldodcerts.pem /etc/apache2/ssl/certs

RUN curl -O 'http://172.17.0.1:8081/repository/npm/login-json/-/login-json-0.1.0.tgz'
RUN tar xvf login-json*
RUN cp -rf package/dist /var/www/frontend

RUN pip3 install flask flask-cors
RUN pip3 install python-ldap pyjwt
RUN pip3 install --trusted-host 172.17.0.1 --index-url='http://172.17.0.1:8081/repository/python/simple' userManagement 

RUN dos2unix startup.sh
CMD ./startup.sh
